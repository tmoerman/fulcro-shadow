(ns fulcro-shadow.demo-ws
  (:require [fulcro.client.primitives :as fp :refer [defsc]]
            [fulcro.client.localized-dom :as dom]
            [fulcro-css.css :as css]
            [nubank.workspaces.core :as ws]
            [nubank.workspaces.card-types.fulcro :as ct.fulcro]
            [nubank.workspaces.lib.fulcro-portal :as f.portal]
            [fulcro.client.mutations :as m :refer [defmutation]]
            [fulcro.client.primitives :as prim]
            ["@blueprintjs/core" :refer [Button ButtonGroup AnchorButton]]))

(defsc FulcroDemo
  [this {:keys [counter]}]
  {:initial-state (fn [_] {:counter 0})
   :ident         (fn [] [::id "singleton"])
   :query         [:counter]}
  (dom/div
    (str "Fulcro counter demo --> [" counter "]")
    (dom/button {:onClick #(m/set-value! this :counter (inc counter))} "+")))

;;
;; Assignment:
;; ===========
;; create a stateful component with state determining which
;;

(defn bp-button-group [& args] (dom/macro-create-element ButtonGroup args))
(defn bp-button [& args] (dom/macro-create-element Button args))
(defn bp-anchor-button [& args] (dom/macro-create-element AnchorButton args))

(defsc MyButtonGroup [this {:keys [ui/dark?] :as props} computed {:keys [grn red blu]}]
  {:query         [:ui/dark?]
   :ident         (fn [] [::id "singleton"])
   :initial-state (fn [_] {:ui/dark? false})
   :css           [[:.grn {:background-color "green"}]
                   [:.blu {:background-color "blue"}]
                   [:.red {:background-color "red"}]]}
  (dom/div
    (dom/div {:className grn} "green")
    (dom/div {:className red} "red")
    (dom/div {:className blu} "blue")
    (bp-button-group (if dark? {:className "bp3-dark"} {})
      (bp-button {:icon    (if dark? "moon" "flash")
                  :text    (if dark? "dark Marc" "nikkel Nico")
                  :onClick (fn [evt] (m/toggle! this :ui/dark?))})
      (bp-button {:icon "function"
                  :text "functions"})
      (bp-anchor-button {:rightIcon "caret-down"
                         :text      "options"}))))

(ws/defcard fulcro-toggle-light-dark
  (ct.fulcro/fulcro-card
    {::f.portal/root MyButtonGroup}))

(css/upsert-css "my-css" MyButtonGroup)