(ns fulcro-shadow.workspaces
  (:require
    [nubank.workspaces.core :as ws]
    [fulcro-shadow.demo-ws]
    [fulcro-shadow.mutations]))

(defonce init (ws/mount))
